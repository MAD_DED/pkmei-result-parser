const DEBUG = true;
// ==============================================================================================

const http = require('http');
const needle = require('needle');
const fs = require('fs');
const path = require('path');
const readline = require('readline');

// ==============================================================================================

var URL = 'https://www.pkmpei.ru/inform/entrants_list406.html';

let resultCode = '';

const topText = `<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Список поступающих</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="./style.css">
    <link rel="shortcut icon" href="favicon.png" type="image/png"> 
</head>
<body>
<img class="upper-left-image" src="favicon.png"/>
<img class="upper-right-image" src="favicon.png"/>
`;

const bottomText = `
<a class="reload-button" href="#" onClick="window.location.reload();" title="Reload">UPDATE ALL</a>
</body>

</html>`;

// ==============================================================================================

const server = http.createServer((request, response) => {
    
    const p = new Promise((resolve, reject) => {
        needle.get(URL, (err, res) => {
            if (err) {
                throw err;
            }
        
            fs.unlink('downloaded.html', (err_1) => {
                if (!err_1) {
                    // console.log('Buffer file already deleted!');
                }
            });
        
            console.log('Status code:',res.statusCode);
            resultCode = res.body;
            
            let buffer = '';
        
            console.log('Index of table:', resultCode.indexOf(`<table class="thin-grid competitive-group-table"`));
            console.log('Index of end table:', resultCode.indexOf(`</table><br><a name="balls">`));
        
            buffer = resultCode.slice(resultCode.indexOf(`<table class="thin-grid competitive-group-table"`), resultCode.indexOf(`</table><br><a name="balls">`)+8);

            buffer = buffer.replace(`<table class="thin-grid competitive-group-table" border=1 style="width:100%;">`,`<table class="result-table" cellpadding="0" cellspacing="0" border="2" bordercolor="#2c3e50">`);
            buffer = buffer.replace(`style="background-color:#F0F0FF"`, `style="background-color:#636e72; color: #fdcb6e"`);
            buffer = buffer.replace(`<a href="#balls">Баллы*</a>`, `<a href="#balls" style="text-decoration: none; color: #fdcb6e">Баллы*</a>`);
            buffer = buffer.replace(`<tr style="background-color:#F0F0FF">`, `<tr style="background-color:#636e72; color: #fdcb6e">`);

            for (let i = -2; i < 12; i++){
                if (!(i < 0)) {
                    buffer = buffer.replace(`<tr id`, `<tr class="list-item highlighted" id`);
                    buffer = buffer.replace(`<td></td>`,`<td style="text-align: right;"><---${i+1}</td>`);
                }
            }

            while(buffer.indexOf(`<tr id`) != -1){
                buffer = buffer.replace(`<tr id`,`<tr class="list-item defeated" id`);
            }

            // console.log('Table content:', buffer);
        
            let htmlText = topText + '\n' + buffer + '\n' + bottomText;
        
            fs.writeFile('downloaded.html', htmlText, (err_2) => {
                if (err_2) {
                    reject(err_2);
                }
        
                console.log('Downloaded data saved!');
                resolve();
            });
        });
    });

    p.then(() => {
        let filePath = path.join(__dirname, request.url === '/' ? 'downloaded.html' : request.url);
        const ext = path.extname(filePath);
        let contentType = 'text/html';

        console.log('Request = ', request.url);
    
        switch (ext) {
            case '.css':
                contentType = 'text/css';
                break;
            case '.js':
                contentType = 'text/javascript';
                break;
            case '.png':
                contentType = 'image/png';
                break;
            default:
                contentType = 'text/html';
                break;
        }

        fs.readFile(filePath, (err, content) => {
            if (err) {
                response.writeHead(500);
                response.end('Error, file does not exist!');
            }
            else {  
                response.writeHead(200, {
                    'Content-Type': contentType
                });
                response.end(content);
            }
        });
    }).catch(() => {
        console.log('Error was detected! Halt!');
    });
});

// ==============================================================================================

// ==============================================================================================

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

let PORT = process.env.PORT || 3000;

if (!DEBUG) {
    rl.question(`Select custom port (default = ${PORT}):`, (answer) => {
        if (answer) {
            if (Number(answer) != NaN) {
                PORT = Number(answer)
            }
        }
        server.listen(PORT, () => {
            console.log(`Server has been started on ${PORT}...`);
        });
        rl.close()
    });
}
else {
    server.listen(PORT, () => {
        console.log(`Server has been started on ${PORT}...`);
    });
}

// ==============================================================================================